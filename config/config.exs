# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# Customize non-Elixir parts of the firmware.  See
# https://hexdocs.pm/nerves/advanced-configuration.html for details.
config :nerves, :firmware, rootfs_overlay: "rootfs_overlay"

# Use shoehorn to start the main application. See the shoehorn
# docs for separating out critical OTP applications such as those
# involved with firmware updates.
config :shoehorn,
  init: [:nerves_runtime, :nerves_init_gadget],
  app: Mix.Project.config()[:app]

# Import target specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
# Uncomment to use target specific configurations

# import_config "#{Mix.Project.config[:target]}.exs"

config :nerves_network,
  regulatory_domain: "US"

config :robo_run, interface: :wlan0

key_mgmt = System.get_env("NERVES_NETWORK_KEY_MGMT") || "WPA_PSK"

config :nerves_network, :default,
  wlan0: [
    ssid: System.get_env("WIFI_SSID"),
    psk: System.get_env("WIFI_PSK"),
    # key_mgmt: String.to_atom(key_mgmt),
  ]


config :nerves_firmware_ssh,
  authorized_keys: [
    File.read!(Path.join(System.user_home!(), ".ssh/id_rsa.pub"))
  ]

config :logger,
  backends: [RingLogger]

config :logger, level: :debug

config :nerves_init_gadget,
  node_name: :robo_run,
  mdns_domain: "robo_run.local"

config :robo_run, led_pin: 26

config :gen_tcp, ip: {127,0,0,1},port: 4040
