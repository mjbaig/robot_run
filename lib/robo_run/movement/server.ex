defmodule RoboRun.Movement.Server do

  use GenServer

  alias RoboRun.Movement.ArduinoMovement

  require Logger

  def start_link() do
    GenServer.start_link(__MODULE__, [], name: :arduino_movement)
  end

  def init(_) do
    ArduinoMovement.start()
  end

  def handle_call({:move_up, speed}, _from, movement_pid) do
    Logger.debug("Executing gen server call to move up")
    {:ok, movement_map} =  ArduinoMovement.move_up(movement_pid, speed)
    {:reply, :ok, movement_pid}
  end

  def handle_call({:move_left, speed}, _from, movement_pid) do
    Logger.debug("Executing gen server call to move left")
    {:ok, movement_map} = ArduinoMovement.move_left(movement_pid, speed)
    {:reply, :ok, movement_pid}
  end

  def handle_call({:move_right, speed}, _from, movement_pid) do
    Logger.debug("Executing gen server call to move right")
    {:ok, movement_map} = ArduinoMovement.move_right(movement_pid, speed)
    {:reply, :ok, movement_pid}
  end

  def handle_call({:move_back, speed}, _from, movement_pid) do
    Logger.debug("Executing gen server call to move back")
    {:ok, movement_map} = ArduinoMovement.move_back(movement_pid, speed)
    {:reply, :ok, movement_pid}
  end

end
