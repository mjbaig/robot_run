defmodule RoboRun.Api.Controllers.MovementController do

  import Plug.Conn

  require Logger

  def move_up(conn) do

    Logger.debug "Moving up"

    genserver_response = GenServer.call(:arduino_movement, {:move_up, 100})

    IO.puts(genserver_response)

    conn
      |> put_resp_content_type("application/json")
      |> send_resp(200, Poison.encode!(%{:message => "moving up"}))
      |> halt()
  end

  def move_back(conn) do

    Logger.debug "Moving back"

    genserver_response = GenServer.call(:arduino_movement, {:move_back, 100})

    IO.puts(genserver_response)

    conn
      |> put_resp_content_type("application/json")
      |> send_resp(200, Poison.encode!(%{:message => "moving back"}))
      |> halt()
  end

  def move_left(conn) do

    Logger.debug "Moving left"

    genserver_response = GenServer.call(:arduino_movement, {:move_left, 100})

    IO.puts(genserver_response)

    conn
      |> put_resp_content_type("application/json")
      |> send_resp(200, Poison.encode!(%{:message => "moving left"}))
      |> halt()
  end

  def move_right(conn) do

    Logger.debug "Moving right"

    genserver_response = GenServer.call(:arduino_movement, {:move_right, 100})

    IO.puts(genserver_response)

    conn
      |> put_resp_content_type("application/json")
      |> send_resp(200, Poison.encode!(%{:message => "moving right"}))
      |> halt()
  end

end
