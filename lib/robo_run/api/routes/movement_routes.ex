defmodule RoboRun.Api.Routes.MovementRoutes do

  use Plug.Router

  plug(:match)

  plug(:dispatch)

  plug(Plug.Logger)

  get("/", do: send_resp(conn, 200, Poison.encode!(%{:route_world => "WHO ARE YOU PEOPLE?"})))

  get("/movement/up", do: RoboRun.Api.Controllers.MovementController.move_up(conn))

  get("/movement/back", do: RoboRun.Api.Controllers.MovementController.move_back(conn))

  get("/movement/left", do: RoboRun.Api.Controllers.MovementController.move_left(conn))

  get("/movement/right", do: RoboRun.Api.Controllers.MovementController.move_right(conn))

end
