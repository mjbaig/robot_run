defmodule RoboRun.Tcp.Controller.MovementServer do

  use GenServer

  require Logger

  @ip_address Application.get_env :tcp_server, :ip, {127,0,0,1}
  @server_port Application.get_env :tcp_server, :port, 4040

  def start_link() do
    Logger.debug "Starting TCP GenServer"
    GenServer.start_link(__MODULE__, [@ip_address, @server_port],  name: :tcp_movement_server)
  end

  def init [ip_address,server_port] do
    {:ok,listen_socket}= :gen_tcp.listen(server_port,[:binary,{:packet, 0},{:active,true}])
    {:ok,socket } = :gen_tcp.accept listen_socket
    {:ok, %{ip: ip_address,port: server_port,socket: socket}}
    Logger.debug "TCP GenServerInitialized"
  end

  def handle_info({:tcp,socket,packet},state) do
    Logger.debug "Handling TCP request"
    IO.inspect packet, label: "incoming packet"
    cond do
      packet == "move_up\r\n" ->
        :gen_tcp.send socket,"Moving up\n"
      true ->
        :gen_tcp.send socket,"You done goofed A-a-ron\n"
    end

    {:noreply,state}
  end

  def handle_info({:tcp_closed,socket},state) do
    IO.inspect "Socket has been closed"
    {:noreply,state}
  end

  def handle_info({:tcp_error,socket,reason},state) do
    IO.inspect socket,label: "connection closed dut to #{reason}"
    {:noreply,state}
  end

end
