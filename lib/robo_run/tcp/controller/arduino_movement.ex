defmodule RoboRun.Tcp.Controller.ArduinoMovement do

  require Logger

  def accept(port) do

    {:ok, socket} = :gen_tcp.listen(port, [:binary, packet: :line, active: false, reuseaddr: true])

    Logger.debug("Accepting connections on #{port}")

    loop_acceptor(socket)

  end

  defp loop_acceptor(socket) do
    {:ok, client} = :gen_tcp.accept(socket)
    Logger.debug "Starting task supervisor"

    Task.async(fn -> serve(client) end)

    loop_acceptor(socket)
  end

  defp serve(socket) do
    socket
      |> read_line()
      |> execute_movement(socket)

      serve(socket)
  end

  defp read_line(socket) do
    {:ok, line} = :gen_tcp.recv(socket, 0)
    line
  end

  defp write_line(line, socket) do
    :gen_tcp.send(socket, line)
  end

  defp execute_movement(line, socket) do

    IO.puts line

    cond do
      line == "move_up\n" ->
        :gen_tcp.send(socket, "Moving Up")
      line == "move_back\n" ->
        :gen_tcp.send(socket, "Moving Back")
      line == "move_right\n" ->
        :gen_tcp.send(socket, "Moving Right")
      line == "move_left\n" ->
        :gen_tcp.send(socket, "Moving Left")
      true ->
        :gen_tcp.send(socket, Poison.encode!(%{:message => "invalid command"}))
    end
  end

end
