defmodule RoboRun.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  require Logger

  @target Mix.Project.config()[:target]

  import Supervisor.Spec

  def start(_type, _args) do

    Logger.debug "Starting Application"

    opts = [strategy: :one_for_one, name: RoboRun.Supervisor]

    Supervisor.start_link(children(@target), opts)

    {:ok, self()}

  end

  # List all child processes to be supervised
  def children("host") do
    [
      Plug.Adapters.Cowboy.child_spec(:http, RoboRun.Api.Controllers.MovementRoutes, [], port: 8080),
    ]
  end

  def children(_target) do
    [
      worker(RoboRun.Movement.Server, []),
      worker(RoboRun.Tcp.Controller.MovementServer, []),
      Plug.Adapters.Cowboy.child_spec(:http, RoboRun.Api.Routes.MovementRoutes, [], port: 8080),
    ]
  end

end
